﻿#region

using System;
using Minesweeper.API;
using Minesweeper.API.Extensions;
using Minesweeper.Components;
using UnityEngine;
using UnityEngine.UI;

#endregion

namespace Minesweeper.Managers
{
    public class GameManager : GameElementBase
    {
        private GameSolver _solver;

        [SerializeField] private GameGrid _grid;
        
        [SerializeField] private GameObject _statsPanel;
        
        [SerializeField] private GameObject _toolBarPanel;
        
        [SerializeField] private GameObject _autoSolveButton;

        [SerializeField] private Text _statsText;

        [SerializeField] private Text _widthText;
        
        [SerializeField] private Text _heightText;
        
        [SerializeField] private Text _minesText;

        [SerializeField] private GridLayoutGroup _gridLayout;
        
        private void ShowGameStats(GameResult stats)
        {
            switch (stats.GameStatus)
            {
                case GameStatus.Completed:
                    _statsText.text = string.Format(
                        "<color=green>SUCCESS!</color>{6}" +
                        "Total Cells: {0}{6}" + 
                        "Cells Revealed: {1}{6}" +
                        "Flagged Mine Cells: {2}{6}" +
                        "Mines: {3}{6}" +
                        "Cells Revealed (%): {4}{6}" +
                        "Mines Flagged (%): {5}"
                        , stats.TotalCells
                        , stats.CellsRevealed
                        , stats.FlaggedMineCells
                        , stats.Mines
                        , stats.PercentCellsRevealed
                        , stats.PercentMinesFlagged
                        , Environment.NewLine);
                    
                    ShowStatsPanel();
                    break;
                
                case GameStatus.Failed:
                    _statsText.text = "<color=red>FAILED!</color>";
                    
                    ShowStatsPanel();
                    break;
            }
        }

        public void ShowStatsPanel()
        {
            _toolBarPanel.SetActive(false);
            _statsPanel.SetActive(true);
        }

        public void HideStatsPanel()
        {
            _statsPanel.SetActive(false);
            _toolBarPanel.SetActive(true);
        }

        private void UpdateSettings()
        {
            Settings.Width = _widthText.text.ToInt(Settings.Width);
            Settings.Height = _heightText.text.ToInt(Settings.Height);
            Settings.MinesCount = _minesText.text.ToInt(Settings.MinesCount);
            Settings.Validate();

            _gridLayout.constraintCount = Settings.Width;
        }

        public void NewGame()
        {
            UpdateSettings();
            
            _solver.Grid.NewGame();
            
            _autoSolveButton.SetActive(true);
        }

        public void Solve()
        {
            _autoSolveButton.SetActive(false);
            var stats = _solver.Solve();
            
            ShowGameStats(stats);
        }

        protected override void Awake()
        {
            base.Awake();

            _autoSolveButton.SetActive(false);
            _solver = new GameSolver(_grid, Settings);
        }

        protected override void Start()
        {
            base.Start();

            NewGame();
        }
    }
}