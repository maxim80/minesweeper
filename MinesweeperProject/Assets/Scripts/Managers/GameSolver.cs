﻿#region

using Minesweeper.API;
using Minesweeper.API.Extensions;
using Minesweeper.Components;
using UnityEngine;
using Random = System.Random;

#endregion

namespace Minesweeper.Managers
{
    public class GameSolver
    {
        public GameGrid Grid { get; private set; }

        public GameSettings Settings { get; private set; }

        private readonly Random _random = new Random();

        public GameSolver(GameGrid grid, GameSettings settings)
        {
            Grid = grid;
            Settings = settings;
        }

        public GameResult Solve()
        {
            while (Grid.Status == GameStatus.InProgress)
            {
                if (Grid.Cells.CountCells(GameCellExtensions.IsRevealed) < 1)
                {
                    FirstMove();
                }
                
                FlagObviousMines();

                if (HasAvailableMoves())
                {
                    ObviousNumbers();
                }
                else
                {
                    RandomMove();
                }

                Endgame();
            }

            var stats = Grid.GetStats();
            return stats;
        }

        private void FirstMove()
        {
            var randomX = _random.Next(1, Settings.Width - 1);
            var randomY = _random.Next(1, Settings.Height - 1);

            Grid.FirstMove(randomX, randomY, _random);
            Grid.RevealCell(randomX, randomY);
        }

        private void RandomMove()
        {
            var randomId = _random.Next(1, Grid.Cells.Length);
            var panel = Grid.Cells.GetCell(randomId);
            
            while (panel.IsRevealed || panel.IsFlagged)
            {
                randomId = _random.Next(1, Grid.Cells.Length);
                panel = Grid.Cells.GetCell(randomId);
            }

            Grid.RevealCell(panel.X, panel.Y);
        }

        private bool HasAvailableMoves()
        {
            var numberedCells = Grid.Cells.GetNumberedCells();
            
            foreach (var cell in numberedCells)
            {
                var neighborCells = Grid.Cells.GetNeighbors(cell.X, cell.Y);
                var flaggedNeighborCells = neighborCells.GetFlaggedCells();
                
                if (flaggedNeighborCells.Count != cell.AdjacentMines)
                {
                    continue;
                }
                if (neighborCells.CountHidenCells() < 1)
                {
                    continue;
                }
                
                return true;
            }
            return false;
        }

        private void ObviousNumbers()
        {
            var numberedCells = Grid.Cells.GetNumberedCells();
            
            foreach (var numberedCell in numberedCells)
            {
                var neighborCells = Grid.Cells.GetNeighbors(numberedCell.X, numberedCell.Y);
                var flaggedNeighborCells = neighborCells.GetFlaggedCells();

                if (flaggedNeighborCells.Count != numberedCell.AdjacentMines)
                {
                    continue;
                }

                var hiddenCells = neighborCells.GetHidenCells();
                foreach (var hiddenPanel in hiddenCells)
                {
                    Grid.RevealCell(hiddenPanel.X, hiddenPanel.Y);
                }
            }
        }

        private void FlagObviousMines()
        {
            var numberedCells = Grid.Cells.GetNumberedCells();
            
            foreach (var cell in numberedCells)
            {
                var neighborPanels = Grid.Cells.GetNeighbors(cell.X, cell.Y);
                if (neighborPanels.CountUnrevealedCells() != cell.AdjacentMines)
                {
                    continue;
                }
                
                foreach (var neighbor in neighborPanels.GetUnrevealedCells())
                {
                    Grid.FlagCell(neighbor.X, neighbor.Y);
                }
            }
        }

        private void Endgame()
        {
            var flaggedCells = Grid.Cells.CountFlaggedCells();
            if (flaggedCells != Settings.MinesCount)
            {
                return;
            }
            
            var hiddenCells = Grid.Cells.GetHidenCells();
            foreach (var cell in hiddenCells)
            {
                Grid.RevealCell(cell);
            }
        }
    }
}