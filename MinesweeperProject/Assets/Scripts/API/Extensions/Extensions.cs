﻿using System.Collections.Generic;

namespace Minesweeper.API.Extensions
{
    public static class GenericExtensions
    {
        public static int ToInt(this string src, int fallbackValue = 0)
        {
            if (src.IsNullOrEmpty())
            {
                return fallbackValue;
            }

            int.TryParse(src, out fallbackValue);
            return fallbackValue;
        }

        public static bool IsNullOrEmpty(this string src)
        {
            var res = string.IsNullOrEmpty(src);
            return res;
        }
        
        public static bool IsNullOrEmpty<T>(this IList<T> src)
        {
            var res = src == null || src.Count < 1;
            return res;
        }
    }
}