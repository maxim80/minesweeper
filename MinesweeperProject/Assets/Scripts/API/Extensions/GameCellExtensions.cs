﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Minesweeper.Components;
using UnityEngine;
using Random = System.Random;

namespace Minesweeper.API.Extensions
{
    public static class GameCellExtensions
    {
        public static GameCell GetCell(this IList<GameCell> src
            , int id)
        {
            if (src.IsNullOrEmpty())
            {
                Debug.LogError("'src' paramter is null\\empty.");
                return null;
            }

            foreach (var cell in src)
            {
                if (!cell.Equals(id))
                {
                    continue;
                }

                return cell;
            }
            return null;
        }
        
        public static GameCell GetCell(this IList<GameCell> src
            , int x
            , int y)
        {
            if (src.IsNullOrEmpty())
            {
                Debug.LogError("'src' paramter is null\\empty.");
                return null;
            }

            foreach (var cell in src)
            {
                if (!cell.Equals(x, y))
                {
                    continue;
                }

                return cell;
            }
            return null;
        }
        
        public static IList<GameCell> GetCells(this IList<GameCell> src
            , Predicate<GameCell> condition)
        {
            if (src.IsNullOrEmpty())
            {
                Debug.LogError("'src' paramter is null\\empty.");
                return null;
            }
            if (condition == null)
            {
                Debug.LogError("'condition' paramter is null\\empty.");
                return null;
            }
            
            var res = new List<GameCell>();
            foreach (var cell in src)
            {
                var condRes = condition(cell);
                if (!condRes)
                {
                    continue;
                }
                res.Add(cell);
            }
            return res;
        }

        private static bool IsNeighbor(this GameCell src
            , int x
            , int y
            , int depth)
        {
            var res = src.X >= x - depth 
                      && src.X <= x + depth
                      && src.Y >= y - depth 
                      && src.Y <= y + depth;
            return res;
        }
        
        public static IList<GameCell> GetNeighbors(this IList<GameCell> src
            , int x
            , int y
            , int depth = 1
            , Predicate<GameCell> predicate = null)
        {
            if (src.IsNullOrEmpty())
            {
                Debug.LogError("'src' paramter is null\\empty.");
                return null;
            }

            var res = new List<GameCell>();
            foreach (var cell in src)
            {
                var isNeighbor = cell.IsNeighbor(x, y, depth);
                if (!isNeighbor)
                {
                    continue;
                }
                if (predicate != null)
                {
                    var accepted = predicate(cell);
                    if (!accepted)
                    {
                        continue;
                    }
                }
                res.Add(cell);
            }
            return res;
        }

        public static int GetMinesCount(this IList<GameCell> src)
        {
            if (src.IsNullOrEmpty())
            {
                Debug.LogError("'src' paramter is null\\empty.");
                return -1;
            }

            var count = 0;
            foreach (var cell in src)
            {
                if (!cell.IsMine)
                {
                    continue;
                }
                count++;
            }
            return count;
        }

        public static int CountCells(this IList<GameCell> src,
            Predicate<GameCell> condition)
        {
            if (src.IsNullOrEmpty())
            {
                Debug.LogError("'src' paramter is null\\empty.");
                return -1;
            }
            if (condition == null)
            {
                Debug.LogError("'condition' paramter is null\\empty.");
                return -1;
            }
            
            var count = 0;
            foreach (var cell in src)
            {
                if (!condition(cell))
                {
                    continue;
                }
                count++;
            }
            return count;
        }
        
        public static IList<GameCell> ExcludeCells(this IList<GameCell> src, IList<GameCell> items)
        {
            if (src.IsNullOrEmpty())
            {
                Debug.LogError("'src' paramter is null\\empty.");
                return null;
            }
            if (items.IsNullOrEmpty())
            {
                Debug.LogError("'items' paramter is null\\empty.");
                return null;
            }

            var clone = new List<GameCell>();
            foreach (var cell in src)
            {
                if (items.Contains(cell))
                {
                    continue;
                }
                
                clone.Add(cell);
            }
            return clone;
        }

        public static IEnumerable<GameCell> RandomizeCellsOrder(this IList<GameCell> src, int count, Random random)
        {
            if (src.IsNullOrEmpty())
            {
                Debug.LogError("'src' paramter is null\\empty.");
                return null;
            }
            
            var clone = new List<GameCell>();
            while (clone.Count < count)
            {
                var index = random.Next(0, src.Count - 1);
                var cell = src[index];
                if (clone.Contains(cell))
                {
                    continue;
                }
                clone.Add(cell);
            }
            return clone;
        }

        public static IEnumerable<GameCell> GetNumberedCells(this IList<GameCell> src)
        {
            var numberedCells = src.GetCells(IsReveledWithNeighborMines);
            return numberedCells;
        }

        public static IList<GameCell> GetFlaggedCells(this IList<GameCell> src)
        {
            var flaggedCells = src.GetCells(IsFlagged);
            return flaggedCells;
        }

        public static bool NotMine(this GameCell cell)
        {
            return !cell.IsMine;
        }
        
        public static bool IsRevealed(this GameCell cell)
        {
            return cell.IsRevealed;
        }

        public static bool IsUnrevealed(this GameCell cell)
        {
            return !cell.IsRevealed;
        }

        public static bool IsHidenUnmined(this GameCell cell)
        {
            return cell.IsUnrevealed() && cell.NotMine();
        }

        private static bool IsReveledWithNeighborMines(this GameCell cell)
        {
            return cell.IsRevealed && cell.AdjacentMines > 0;
        }

        private static bool IsHidenUnflagged(this GameCell cell)
        {
            return !cell.IsRevealed && !cell.IsFlagged;
        }

        private static bool IsFlagged(this GameCell cell)
        {
            return cell.IsFlagged;
        }
        
        public static int CountUnrevealedCells(this IList<GameCell> src)
        {
            return src.CountCells(IsUnrevealed);
        }
        
        public static IEnumerable<GameCell> GetUnrevealedCells(this IList<GameCell> src)
        {
            var unrevealedCells = src.GetCells(IsUnrevealed);
            return unrevealedCells;
        }
        
        public static int CountFlaggedCells(this IList<GameCell> src)
        {
            var flaggedCells = src.CountCells(IsFlagged);
            return flaggedCells;
        }

        public static int CountHidenCells(this IList<GameCell> src)
        {
            var res = src.CountCells(IsHidenUnflagged);
            return res;
        }

        public static IEnumerable<GameCell> GetHidenCells(this IList<GameCell> src)
        {
            var hiddenCells = src.GetCells(IsHidenUnflagged);
            return hiddenCells;
        }

        public static bool IsFlaggedMine(this GameCell cell)
        {
            return cell.IsMine && cell.IsFlagged;
        }

        public static bool IsFlaggedOrReleaved(this GameCell cell)
        {
            return cell.IsFlagged || cell.IsRevealed;
        }
    }
}