﻿namespace Minesweeper.API
{
    public class GameResult
    {
        public GameStatus GameStatus { get; set; }
        
        public double TotalCells { get; set; }

        public double CellsRevealed { get; set; }

        public double Mines { get; set; }

        public double FlaggedMineCells { get; set; }

        public double PercentMinesFlagged { get; set; }

        public double PercentCellsRevealed { get; set; }
    }
}