﻿using UnityEngine;

namespace Minesweeper.API
{
    public abstract class GameElementBase : MonoBehaviour
    {
        [SerializeField] private GameSettings _settings;

        public GameSettings Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }

        protected virtual void Awake()
        {
            
        }
        
        protected virtual void Start()
        {
            
        }

        protected virtual void OnDestroy()
        {
            
        }
    }
}