﻿namespace Minesweeper.API
{
    public enum GameStatus
    {
        InProgress,

        Failed,

        Completed
    }
}