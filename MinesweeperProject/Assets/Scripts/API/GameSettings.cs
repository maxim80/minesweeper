﻿#region

using Minesweeper.Components;
using UnityEngine;

#endregion

namespace Minesweeper.API
{
    [CreateAssetMenu(
        fileName = "GameSettings"
        , menuName = "Game Settings")]
    public class GameSettings : ScriptableObject
    {
        private const int MinCellCount = 4;

        private const int MaxCellCount = 10;
        
        [SerializeField] private GameCell _cellPrefab;

        [SerializeField] private Color _flagColor;

        [SerializeField] private Color _mineColor;

        [SerializeField] private Color _revealedColor;
        
        [Range(MinCellCount, MaxCellCount)] [SerializeField] private int _height;

        [Range(MinCellCount, MaxCellCount)] [SerializeField] private int _width;
        
        [Range(MinCellCount, MaxCellCount)] [SerializeField] private int _minesCount;

        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public int MinesCount
        {
            get { return _minesCount; }
            set { _minesCount = value; }
        }

        public GameCell CellPrefab
        {
            get { return _cellPrefab; }
            set { _cellPrefab = value; }
        }

        public Color FlagColor
        {
            get { return _flagColor; }
            set { _flagColor = value; }
        }

        public Color MineColor
        {
            get { return _mineColor; }
            set { _mineColor = value; }
        }

        public Color RevealedColor
        {
            get { return _revealedColor; }
            set { _revealedColor = value; }
        }
        
        private static int Validate(int value, int min, int max, string name)
        {
            if (value < min)
            {
                value = min;
                Debug.LogErrorFormat("'{0}' can't be less then {1}", name, min);
            }
            else if (value > max)
            {
                value = max;
                Debug.LogErrorFormat("'{0}' can't be more then {1}", name, max);
            }
            return value;
        }
        
        public void Validate()
        {
            _width = Validate(_width, MinCellCount, MaxCellCount, "Width");
            _height = Validate(_height, MinCellCount, MaxCellCount, "Height");
            _minesCount = Validate(_minesCount, MinCellCount, MaxCellCount, "MinesCount");

            if (_cellPrefab == null)
            {
                Debug.LogError("'CellPrefab' is null");
            }
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            Validate();
        }
#endif
    }
}