﻿using Minesweeper.API;
using UnityEngine;
using UnityEngine.UI;

namespace Minesweeper.Components
{
    public class GameCell : RawImage
    {
        [SerializeField] private GameSettings _settings;
        
        [SerializeField] private Text _buttonText;
        
        [SerializeField] private int _id;

        public int Id
        {
            get { return _id; }
            private set { _id = value; }
        }

        [SerializeField] private int _x;

        public int X
        {
            get { return _x; }
            private set { _x = value; }
        }

        [SerializeField] private int _y;

        public int Y
        {
            get { return _y; }
            private set { _y = value; }
        }

        [SerializeField] private bool _isMine;

        public bool IsMine
        {
            get { return _isMine; }
            set
            {
                if (_isMine == value)
                {
                    return;
                }
                
                _isMine = value;
                if (!_isMine)
                {
                    return;
                }
                color = _settings.MineColor;
            }
        }

        [SerializeField] private int _adjacentMines;

        public int AdjacentMines
        {
            get { return _adjacentMines; }
            set
            {
                if (_adjacentMines == value)
                {
                    return;
                }
                
                _adjacentMines = value;
                _buttonText.text = value.ToString();
            }
        }

        [SerializeField] private bool _isRevealed;

        public bool IsRevealed
        {
            get { return _isRevealed; }
            set
            {
                if (_isRevealed == value)
                {
                    return;    
                }
                
                _isRevealed = value;
                if (!_isRevealed)
                {
                    return;
                }
                color = _settings.RevealedColor;
            }
        }

        [SerializeField] private bool _isFlagged;

        public bool IsFlagged
        {
            get { return _isFlagged; }
            set
            {
                if (_isFlagged == value)
                {
                    return;
                }
                
                _isFlagged = value;

                if (!_isFlagged)
                {
                    return;
                }
                
                _buttonText.text = "F";
                color = _settings.FlagColor;
            }
        }

        public void Init(int id, int x, int y)
        {
            Id = id;
            X = x;
            Y = y;

            gameObject.name = string.Format("cell_{0} ({1}, {2})", Id, X, Y);
        }

        public bool Equals(int x, int y)
        {
            var res = X == x && Y == y;
            return res;
        }
        
        public bool Equals(int id)
        {
            var res = Id == id;
            return res;
        }

        public override string ToString()
        {
            return gameObject.name;
        }
    }
}