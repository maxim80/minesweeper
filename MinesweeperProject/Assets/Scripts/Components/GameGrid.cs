﻿#region

using System;
using Minesweeper.API;
using Minesweeper.API.Extensions;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

#endregion

namespace Minesweeper.Components
{
    public class GameGrid : GameElementBase
    {
        [SerializeField] private GridLayoutGroup _gridLayout;
        
        [SerializeField] private GameCell[] _cells;

        private const double FirstMoveRow = 0.125;
        
        public GameCell[] Cells
        {
            get { return _cells; }
        }

        [SerializeField] private GameStatus _status;

        public GameStatus Status
        {
            get { return _status; }
        }

        private void CleanBoard()
        {
            if (Cells == null)
            {
                return;
            }

            foreach (var cell in Cells)
            {
                Destroy(cell.gameObject);
            }

            _cells = null;
        }

        public void NewGame()
        {
            CleanBoard();
            
            _cells = new GameCell[Settings.Width * Settings.Height];
            
            var id = 0;
            for (var i = 0; i < Settings.Width; i++)
            {
                for (var j = 0; j < Settings.Height; j++)
                {
                    var cell = Instantiate(Settings.CellPrefab, _gridLayout.gameObject.transform);
                    cell.Init(id + 1, i + 1, j + 1);
                    
                    Cells[id++] = cell;
                }
            }

            _status = GameStatus.InProgress;
        }

        public void RevealCell(GameCell selectedCell)
        {
            selectedCell.IsRevealed = true;
            selectedCell.IsFlagged = false;
            
            if (selectedCell.IsMine) 
            {
                _status = GameStatus.Failed;
            }
            
            if (!selectedCell.IsMine && selectedCell.AdjacentMines == 0)
            {
                RevealZeros(selectedCell.X, selectedCell.Y);
            }
            
            if (!selectedCell.IsMine)
            {
                CompletionCheck();
            }
        }

        public void RevealCell(int x, int y)
        {
            var selectedCell = Cells.GetCell(x, y);
            RevealCell(selectedCell);
        }

        public void FirstMove(int x, int y, Random rand)
        {
            var depth = FirstMoveRow * Settings.Width;
            var neighbors = Cells.GetNeighbors(x, y, (int) depth);
            var currentCell = Cells.GetCell(x, y);
            neighbors.Add(currentCell);

            var mineList = Cells.ExcludeCells(neighbors).RandomizeCellsOrder(Settings.MinesCount, rand);
            foreach (var mineCell in mineList)
            {
                mineCell.IsMine = true;
            }

            var openCells = Cells.GetCells(GameCellExtensions.NotMine);
            foreach (var openCell in openCells)
            {
                var nearbyPanels = Cells.GetNeighbors(openCell.X, openCell.Y);
                openCell.AdjacentMines = nearbyPanels.GetMinesCount();
            }
        }

        private void RevealZeros(int x, int y)
        {
            var neighborCells = Cells.GetNeighbors(x, y, 1, GameCellExtensions.IsUnrevealed);
            foreach (var neighbor in neighborCells)
            {
                neighbor.IsRevealed = true;
                if (neighbor.AdjacentMines == 0)
                {
                    RevealZeros(neighbor.X, neighbor.Y);
                }
            }
        }

        private void CompletionCheck()
        {
            var hiddenMineCells = Cells.GetCells(GameCellExtensions.IsHidenUnmined);
            if (hiddenMineCells.Count < 1)
            {
                _status = GameStatus.Completed;
            }
        }

        public GameResult GetStats()
        {
            var stats = new GameResult
                            {
                                GameStatus = Status,
                                Mines = Cells.GetMinesCount(),
                                FlaggedMineCells = Cells.CountCells(GameCellExtensions.IsFlaggedMine),
                                TotalCells = Cells.Length,
                                CellsRevealed = Cells.CountCells(GameCellExtensions.IsFlaggedOrReleaved)
                            };

            stats.PercentMinesFlagged = Math.Round(stats.FlaggedMineCells / stats.Mines * 100, 2);
            stats.PercentCellsRevealed = Math.Round(stats.CellsRevealed / stats.TotalCells * 100, 2);

            return stats;
        }

        public void FlagCell(int x, int y)
        {
            var panel = Cells.GetCell(x, y);
            if (!panel.IsRevealed)
            {
                panel.IsFlagged = true;
            }
        }
    }
}